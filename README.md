CodeIgniter&Doctrine&Composer
===

## Version ##
 - CodeIgniter : 2.2.0
 - Doctrine ORM : 2.4.4

## Installation ##

 - Clone the project to (for example) `/var/www/html/ci`
 - Change directory to root project `cd /var/www/html/ci`
 - Run `composer update`
 - Edit database configuration `application/config/database.php`
 - Create database schema `php bin/doctrine.php orm:schema-tool:create`
 - Create a virtual host (for example `ci.vagrant.local`)

```
<VirtualHost *:80>
        ServerName ci.vagrant.local

        DocumentRoot /var/www/html/ci/public

        <Directory "/var/www/html/ci/public">
                AllowOverride All
        </Directory>
</VirtualHost>
```

## Reference ##

 - https://wildlyinaccurate.com/integrating-doctrine-2-with-codeigniter-2
 - https://github.com/EllisLab/CodeIgniter
 - https://github.com/doctrine/doctrine2