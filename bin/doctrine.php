<?php
/**
 * Doctrine CLI bootstrap for CodeIgniter
 *
 * @author  Joseph Wynn <joseph@wildlyinaccurate.com>
 * @link    http://wildlyinaccurate.com/integrating-doctrine-2-with-codeigniter-2
 */
require __DIR__ . '/../vendor/autoload.php';

define('APPPATH', dirname(__FILE__) . '/../application/');
define('BASEPATH', '../vendor/ellislab/codeigniter/system/');
define('ENVIRONMENT', 'development');

chdir(APPPATH);

require __DIR__ . '/../application/libraries/Doctrine.php';

foreach ( $GLOBALS as $helperSetCandidate ) {
	if ($helperSetCandidate instanceof \Symfony\Component\Console\Helper\HelperSet) {
		$helperSet = $helperSetCandidate;
		break;
	}
}

$doctrine = new Doctrine();
$em = $doctrine->em;

$helperSet = new \Symfony\Component\Console\Helper\HelperSet(array (
		'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
		'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
));

\Doctrine\ORM\Tools\Console\ConsoleRunner::run($helperSet);