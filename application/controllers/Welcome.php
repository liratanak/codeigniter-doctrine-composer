<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends BST_Controller {

	public function index() {
		$someObject = new Entity\SomeEntity();
		$someObject->setSomeValue(date("Y/F/d H:i:s"));

		$this->em->persist($someObject);
		$this->em->flush();

		$data = array(
			"object" => $this->em->find("Entity\SomeEntity", 1)
		);

		$data['template'] = __CLASS__.'/'.__FUNCTION__;
		$this->load->view('Layout/default', $data);
	}
}