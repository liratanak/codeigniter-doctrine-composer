<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class ScrollingPage extends BST_Controller {

	public function index() {
		$data['template'] = __CLASS__.'/'.__FUNCTION__;
		$this->load->view('Layout/default', $data);
	}
}