<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

class BST_Controller extends CI_Controller {

	protected $em;

	public function __construct() {
		parent::__construct();

		$this->em = $this->doctrine->em;
	}

}