<!-- Intro Section -->
<section id="intro" class="intro-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1>Example:</h1>
				<code>
					$this->em->find("Entity\SomeEntity", 1) : <?php echo $object->getSomeValue() ?>
				</code>
			</div>
		</div>
	</div>
</section>