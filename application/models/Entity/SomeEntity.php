<?php
namespace Entity;

/**
 * @Entity
 * @Table ( name= "some_table")
 */
class SomeEntity {

	/**
	 * @Id
	 * @Column (type = "integer")
	 * @GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @Column (name = "some_value", type = "string" , nullable = false)
	 */
	protected $someValue;

	/**
	 *
	 * @param mixed $someValue
	 */
	public function setSomeValue($someValue) {
		$this->someValue = $someValue;
	}

	/**
	 *
	 * @return mixed
	 */
	public function getSomeValue() {
		return $this->someValue;
	}
}